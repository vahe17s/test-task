# Use the official Node.js 16 image from the Docker Hub, based on Debian
FROM node:16-buster

# Create app directory in the container
WORKDIR /usr/src/app

# Copy the application files to the container
COPY . .

# Install any application dependencies
# Note: Since we're running a basic HTTP server without external dependencies,
# we don't actually need to run `npm install` for this particular example.
# RUN npm install

# Your app binds to port 8080 so you'll use the EXPOSE instruction to have it mapped by the docker daemon
EXPOSE 7070

# Define the command to run your app using CMD which defines your runtime
CMD [ "node", "app.js" ]
